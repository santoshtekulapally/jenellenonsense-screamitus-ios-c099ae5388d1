//
//  ScreamItus_IOSUI.swift
//  ScreamItus-IOSUI
//
//  Created by santosh tekulapally on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

class ScreamItus_IOSUI: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testcaseone() {
        
        let app = XCUIApplication()
        app.buttons["Calculate"].tap()
        app.otherElements.containing(.staticText, identifier:"Infection Calculator").children(matching: .textField).element.tap()
        app.staticTexts["1000 instructors infected!"].tap()
        
       
      
        
        // 2. test that label exists
        let label = app.staticTexts["1000 instructors infected!"]
        // 5. check that label exists
        XCTAssertNotNil(label)
        // 6. check the text in the label
        XCTAssertEqual("1000 instructors infected!", label.label)
        
        XCTAssertEqual(label.exists, true)
        
        // 1. Find the calculate Button
        let calculatebutton = app.buttons["Calculate"]
        
        // 2. Check that the calculate button exists
        
        XCTAssertEqual(true, calculatebutton.exists)
        
        // 3.. Check that the calculate button is visible
        
        XCTAssertEqual(calculatebutton.exists, true)
        
        
        let Textbox = app.textFields["Plain"]
        
        // 2. Check that the textFields textbox exists
        
        XCTAssertEqual(true, Textbox.exists)
        
        // 3.. Check that the textFields textbox is visible
        
        XCTAssertEqual(Textbox.exists, true)
        
      
        
    }
    
func testcasetwo() {
    
    
    let app = XCUIApplication()
    app.otherElements.containing(.staticText, identifier:"Infection Calculator").children(matching: .textField).element.tap()
    app.buttons["Calculate"].tap()
    app.staticTexts["1000 instructors infected!"].tap()

        
        

    }
    

}
